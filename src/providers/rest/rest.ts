import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  posts: any={};
  data1:Observable<any>;
  apiUrl = 'http://localhost:8000/api';
  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }

  
  
  login(credentials) {
    var header = { "headers": {"Content-Type": "application/json"} };
       
    return new Promise((resolve, reject) => {
     this.http.post(this.apiUrl+'/auth/login', JSON.stringify(credentials),header)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  getUser(data)
  {
    let posts1
    var header = { "headers": {"Content-Type": "application/json"} };
    this.data1=this.http.post(this.apiUrl+'/auth/login', JSON.stringify(data),header)
    
     this.data1.subscribe(resp => {
      console.log(resp);
       posts1 = resp;
      console.log(posts1);
      return posts1;
    });
    return posts1;
  }

  addUser(data) {
    var header = { "headers": {"Content-Type": "application/json"} };
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl+'/auth/login', JSON.stringify(data),header)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
          console.log(err);        
        });
    });
  }

}
