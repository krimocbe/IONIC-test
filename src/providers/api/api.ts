import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  public API_Url :String ='https://jsonplaceholder.typicode.com/users';  
  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  get(query:string=""){
    return this.http.get(this.API_Url+'query');
  }

}
