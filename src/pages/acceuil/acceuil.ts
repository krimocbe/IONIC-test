import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the AcceuilPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-acceuil',
  templateUrl: 'acceuil.html'
})
export class AcceuilPage {

  welcomeRoot = 'WelcomePage'
  restaurantRoot = 'RestaurantPage'
  pharmacieRoot = 'PharmaciePage'
  medecinRoot = 'MedecinPage'
  hotelRoot = 'HotelPage'


  constructor(public navCtrl: NavController) {}

}
