import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  username:String;
  password:String;
  confirm_password:String;
  data:Observable <any>;
  data1:Observable <any>;
  result:any =[];
  result1:any=[];
  constructor(public http : HttpClient ,private toastCtrl: ToastController , public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.data =this.http.get('https://jsonplaceholder.typicode.com/posts/1');
    this.data.subscribe((data)=>
    {
         this.result=data;       
    });
  }

  presentToast(msg:string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }


  register_user(){
    if((this.username.length==0)||(this.password.length==0)||(this.confirm_password.length==0))
    {
      alert("please fill all the gabs");
    } else if(this.password !=this.confirm_password) {
      this.presentToast('votre mot de passe n\'est pas identique' );
    }
    let headers =  new HttpHeaders();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Cont­ent-Type', 'application/json');

    let formdata= new FormData();
    formdata.append('name',"krimou");
    formdata.append('email',"krimo123@gmail.com");
    formdata.append('password',"krimou123");
    
    this.data1=this.http.post('http://192.168.43.13/api/auth/register',formdata,{headers:headers});
    this.data1.subscribe((data1)=>{
        console.log(data1);     
    });
    
  }
/*
  let data = JSON.stringify({

    name: this.registerCredent­ials.name,
    pass: this.registerCredent­ials.password,
    });
    
    let apiloginUrl = 'http://domainename/­user/­login?_format=json';
    let headers = new Headers();
    headers.append('Cont­ent-Type', 'application/json');
    
    this.http.post(apilo­ginUrl, data, {headers: headers})
    .subscribe(data => {
    
    localStorage.setItem­('loggedinData', data['_body']);
    let apiTokenUrl = 'http://domainename/­rest/session/token';
    this.http.get(apiTok­enUrl)
    .subscribe(data => {
    localStorage.setItem­('loggedin_token', data['_body']);
    this.navCtrl.setRoot­(HomePage).then( ()=>{
    this.navCtrl.popToRo­ot().then( ()=> {
    }).catch(err=>{
    console.log(err);
    alert(1 + "- "+err.code+ " "+err.message);
    });
    }).catch(err=>{
    alert(2 + "- "+err.code+ " "+err.message);
    });
    console.log(data);
    }, error => {
    alert(3 + "- "+error.code+ " "+error.message);
    });
    
    }, error => {
    console.log("Access Denied");
    alert("Access Denied");
    });
*/
  ionViewWillEnter(){
    this.data =this.http.get('https://jsonplaceholder.typicode.com/posts/1');
    this.data.subscribe((data)=>
    {
         this.result=data;       
    });
  }

}
