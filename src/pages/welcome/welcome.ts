import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';

import {Component, ViewChild, ElementRef} from '@angular/core';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  constructor(public navCtrl: NavController, public navParams: NavParams ) 
  {
  }

  
  ionViewDidLoad() {
    let latLng = new google.maps.LatLng(45.815011, 15.981919);

    let mapOptions = {
      center: latLng,
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);  
  
  }

}
