import {  Component, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { AcceuilPage } from '../acceuil/acceuil';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RestProvider } from '../../providers/rest/rest';
import { WelcomePage } from '../welcome/welcome';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
 
    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('2000ms ease-in-out')
      ])
    ]),
 
    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),
 
    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ])
    ]),
 
    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class HomePage {

  logoState: any = "in";
  cloudState: any = "in";
  loginState: any = "in";
  formState: any = "in";
  username:string;
  password:string;
  data3:Observable<any>;
  users: any;
  data: any;
  loading: any;
  constructor(public http:HttpClient,public loadingCtrl: LoadingController,public toast:ToastController ,public navCtrl: NavController, public restProvider: RestProvider ) {
  }

  presentToast(msg:string) {
    let toast = this.toast.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  go_register()
  {
    this.navCtrl.push(RegisterPage);
  }

  login() {
    this.showLoader();
    
    let data1 = 
    {
      'email':this.username,
      'password':this.password
    };
    console.log(data1);
    this.restProvider.login(data1).then((result) => {
      this.loading.dismiss();
      this.data = result;
      localStorage.setItem('token', this.data.token);
      //this.navCtrl.setRoot(WelcomePage);
      this.navCtrl.push(AcceuilPage);
    }, (err) => {
      this.loading.dismiss();
      this.presentToast("unauthorised");
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  dologin()
  { 

    let answer={} ;
    let data1 = 
    {
      'email':this.username,
      'password':this.password
    };
    
    console.log(this.restProvider.getUser(data1)); 
    if(this.restProvider.getUser(data1))
     {
      this.presentToast("invalid username or motdepasse");
     }
     else {
    this.navCtrl.push(AcceuilPage);
          }
  }

  

}
