import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PharmaciePage } from './pharmacie';

@NgModule({
  declarations: [
    PharmaciePage,
  ],
  imports: [
    IonicPageModule.forChild(PharmaciePage),
  ],
})
export class PharmaciePageModule {}
