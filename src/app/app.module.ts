import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {AcceuilPage} from '../pages/acceuil/acceuil';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import {HttpClientModule} from '@angular/common/http';
import { RegisterPage } from '../pages/register/register';
import { GoogleMaps } from '@ionic-native/google-maps';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RestProvider } from '../providers/rest/rest';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    AcceuilPage
  ],
  imports: [
  BrowserModule,
  BrowserAnimationsModule, 
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    AcceuilPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    GoogleMaps,
    Geolocation,
    LocationTrackerProvider,
    RestProvider
  ]
})
export class AppModule {}
